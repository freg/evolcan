# eVolcan

<dl>
<dt>Une initiative citoyenne de science participative à la Guadeloupe
  <dd>ayant pour but l'observation et l'exploration de la Soufrière</dd><br/>
</dt>
  <dt>eVolcan est issue du projet <a href="https://framagit.org/freg/openelab">open-eLab</a> </dt>
  <dd>
  <dl><blockquote>
  <dt>aborde les domaines scientifiques, techniques et sociétaux :</dt>
    <dd>des capteurs, de la robotique, de l'informatique embarquée DIY en milieu extrême,</dd>
    <dd>de la collecte, du suivi et de l'analyse des données issues de systèmes complexes,</dd>
    <dd>de la supervision et de la gestion de systèmes autonomes coopérants...</dd>
  <dt>par une mise en valeur citoyenne de la proximité du <a href="http://www.ipgp.fr/~beaudu/download/Poster_OVSG_Soufriere.pdf">volcan actif</a> <a href="http://la1ere.francetvinfo.fr/2014/10/06/le-volcan-de-la-soufriere-en-guadeloupe-pourrait-provoquer-le-meme-type-d-eruption-qu-ontake-au-japon-195984.html">supposé phréatique</a>
  </dt>
  </dl>
  </dd>
</dl>
  
## objectifs
 
   créer une dynamique citoyenne autour du volcan favorisant:
   * la pérennisation des outils et des observations 
   * l'initiation aux techniques DIY applicables aux capteurs et aux systèmes
   * la prise de conscience et l'attention continue sur le phénomène
 
## écosystème
 
 * actuellement 
   * <dt>Dominique géophysicien et responsable du projet DIAPHANE </dt>
     <dd>co inventeur de la technologie des télescopes à MUON</dd>
     <dd>moteur permanent des initiatives d'évolution et de réflexion sur les outils</dd>
   * <dt>open-eLab groupe participatif autour de l'open data</dt>
     <dd>réunissant informaticiens, spécialistes des énergie renouvelables,
     <dd>jeunes chercheurs et des familles
   * <dt>le Fablab de Jarry association de créatifs dans le concret et le ludique</dt>
 * potentiel et souhaitable
   <dt>l'Observatoire volcanologique 
   <dt>l'Université des Antilles 
   <dt>le Rectorat
   <dt><a href="http://www.culturecommunication.gouv.fr/Thematiques/Enseignement-superieur-et-Recherche/Actualites/AMI-2017-Atelier-recherche-culturelle-et-sciences-participatives">Ministère de la rechercher</a></dt>
   

## actions
 
 * <dt>dès début 2016 [sur evolcan](http://evolcan.org) les données de températures
   <dd>ouverture et réflexion permanente sur les données
 * <dt>été 2016 [atelier pitot DIY avec un lycéen](https://framagit.org/freg/openelab/wikis/pitotProbeHandmade) 
   <dd>une première avec des mesures de vitesse des gaz des fumerolles
   <dd>lançant une réflexion sur l'évaluation permanente de l'énergie perceptible
 * <dt>année 2016 rapprochement de eVolcan avec un projet de télescope des écoles
   <dd>et réflexion sur les pistes d'association avec des projets extra territoriaux
 * <dt>année 2016 [ateliers arduino et développement de prototypes de capteurs DIY](https://framagit.org/freg/openelab/wikis/ArduinoAcquisitionSys)
   <dd>un premier capteur sur une colonne de 8 thermocouples est installé 
   <dd>par l'équipe open-elab accompagné par le fablab de Jarry
 * <dt>année 2017 [un second capteur officiel DIAPHANE sur le modèle du premier](https://framagit.org/freg/openelab/wikis/projects/arduino/pt1000_1)
   <dd>le fablab de Jarry participe aux excursions de maintenance
   
## perspectives
 
 * <dt>participer à l'accroissement des solutions d'observation
 * <dt>anticiper la fin des projets de recherche
   <dd>les faire évoluer vers un cycle permanent de formation, initiative, réflexion
   <dd>amener les citoyens de tous ages vers les nouvelles technologies DIY
 * <dt>valoriser la Soufrière en laboratoire naturel citoyen
 * <dt>réinvestir les savoirs et les lieux de savoirs en "open science citoyenne" 
   